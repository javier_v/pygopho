import enum
import os
import stat
import shutil
import time
import threading

def getfile(path):
    fileExts = {}
    allFiles = set(os.path.join(dp, f) for dp, dn, fn in os.walk(os.path.expanduser(pathToPhotos)) for f in fn)

    for photoObj in allFiles:
        pathToObj = f'{photoObj}'
        ext = photoObj[pathToObj.rfind(".") + 1:]
        fileExts[ext] = fileExts.get(ext,0) + 1
    return fileExts, list(allFiles)

def copy_files(fileList,numThread,threads):
    stepFiles = int(len(fileList)/threads)
    if numThread == (threads - 1):
        filesToCopy = fileList[numThread*stepFiles:]
    else:
        filesToCopy = fileList[numThread*stepFiles:(numThread + 1) * stepFiles]


    start = time.perf_counter()
    for file in filesToCopy:
        ext = file[file.rfind(".")+1:]
        fileName = file[file.rfind("/")+1:]
        finalPath = os.path.join(pathCons,ext,fileName)
        try:
            shutil.copy(file,finalPath)
        except FileExistsError: 
            print(f'file {file} already exists')
        except Exception as e:
            print(f'trono porque {e}')
            quit()
    end = time.perf_counter()
    print(f'thread {numThread} done, files copied {len(filesToCopy)} in {round(end-start,2)} secs')




#pathToPhotos = '/mnt/d/OnlineDataZips/Backups/GooglePhotos'
pathToPhotos = '/mnt/c/Users/jvrvz/Documents/Takeout'

if __name__ == "__main__":
    mode = 0o666
    fileExts,filesList = getfile(pathToPhotos)
        
    pathCons = os.path.join(pathToPhotos,'consolidated')
    try:
        os.mkdir(pathCons,mode)
    except FileExistsError:
        print(f'folder {pathCons} already exists')

    for ext, value in  fileExts.items():
        path_newdir = os.path.join(pathCons,ext)
        try:
            os.mkdir(path_newdir, mode)
        except FileExistsError:
            print(f'folder {path_newdir} already exists')
            continue


    threads = []
    totThreads = 500
    for numThread in range(totThreads):
        t = threading.Thread(target=copy_files, args = (filesList,numThread,totThreads))
        t.start()
        threads.append(t)

    for thread in threads:
        thread.join()


