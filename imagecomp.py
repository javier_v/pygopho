import cv2
from skimage.metrics import structural_similarity as ssim
import numpy as np

def mse(imageA, imageB):
    err = np.sum((imageA.astype("float") - imageB.astype("float")) ** 2)
    err /= float(imageA.shape[0] * imageA.shape[1])

    return (err)**(0.5)

jpg_path = '/mnt/c/Users/jvrvz/Documents/Takeout/consolidated/jpeg/'

imageA = jpg_path + '/' + 'IMG_166429820376786.jpeg'
imageB = jpg_path + '/' + 'IMG_166429820376786-edited.jpeg'



papa1_img = cv2.resize(cv2.imread(imageA),(1000,1000),interpolation = cv2.INTER_AREA)
papa2_img = cv2.resize(cv2.imread(imageB),(1000,1000),interpolation = cv2.INTER_AREA)



print(papa1_img.shape)
print(papa2_img.shape)

#image = cv2.rotate(papa1_img, cv2.cv2.ROTATE_90_CLOCKWISE)
#ROTATE_90_COUNTERCLOCKWISE

comps_mse = mse(cv2.cvtColor(papa1_img,cv2.COLOR_BGR2GRAY),cv2.cvtColor(papa2_img,cv2.COLOR_BGR2GRAY))
comps_ssim = ssim(cv2.cvtColor(papa1_img,cv2.COLOR_BGR2GRAY),cv2.cvtColor(papa2_img,cv2.COLOR_BGR2GRAY))


print(comps_ssim)
print(comps_mse)

